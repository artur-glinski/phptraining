<?php
require_once __DIR__ . '/../core/structured/calculator.php';
require_once __DIR__ . '/../core/structured/error/appLogicErrorLib.php';
require_once __DIR__ . '/../core/structured/templates/templateEngine.php';

$calculatorResult = runCalculator();

showcalculatorMain($calculatorResult);