<?php

define('OPERATOR_PLUS', '+');
define('OPERATOR_MINUS', '-');
define('OPERATOR_MULTIPLY', '*');
define('OPERATOR_DIVIDE', '/');

define('REQUEST_DATA_MISSING', 'Nie przekazano wymaganych danych. Brak ');
define('REQUEST_DATA_INCORRECT', 'Przekazano niepoprawne dane dla ');
define('REQUEST_DATA_CANT_BE_ZERO_OR_EMPTY', 'Zmienna nie może przyjmować wartości zero dla wybranej operacji.');

define('FIRST_DATA_ALIAS', 'A');
define('SECOND_DATA_ALIAS', 'B');
define('OPERATOR_ALIAS', 'RODZAJ OPERACJI');
