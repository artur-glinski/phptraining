<?php

require_once ('/../config/dictionary.php');
require_once ('/../error/appLogicErrorLib.php');

function validateData($varName1, $varName2) {
	validateVariable($varName1);
	validateVariable($varName2);
	validateVariableNonZero($varName2);
	return $GLOBALS['errors'];
}

function validateVariable($varName) {
	if (array_key_exists($varName, $_REQUEST) === true) {
		if (is_numeric($_REQUEST[$varName]) === false && $_REQUEST[$varName] !== '') {
			addError(REQUEST_DATA_INCORRECT . $varName);
			return false;
		}
	}
	return true;
}

function validateVariableNonZero($varName) {
	if (OPERATOR_DIVIDE === $_REQUEST['operation'] && (int)$_REQUEST[$varName] === 0) {
		addError(REQUEST_DATA_CANT_BE_ZERO_OR_EMPTY);
		return false;
	}
	return true;
}