<?php

require_once(__DIR__ . '/../config/dictionary.php');

function requestHasData() {
	return (bool)count($_REQUEST);
}

function getDataFromRequest($varName) {
	if ($_REQUEST[$varName] === '') {
		addError(REQUEST_DATA_MISSING . $varName);
		return null;
	}
	return $_REQUEST[$varName];
}

function getResult($var1, $var2, $operator) {
		if (count($GLOBALS['errors']) === 0) {

		switch ($operator) {
			case OPERATOR_PLUS:
				$result = $var1 + $var2;
				break;
			case OPERATOR_MINUS:
				$result = $var1 - $var2;
				break;
			case OPERATOR_MULTIPLY:
				$result = $var1 * $var2;
				break;
			case OPERATOR_DIVIDE:
				$result = $var1 / $var2;
				break;
		}
	return $result;
	}
}