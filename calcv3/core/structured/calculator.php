<?php

require_once ('config/dictionary.php');
require_once ('request/requestToolsLib.php');
require_once ('validators/validatorsLib.php');

function runCalculator() {
	if(requestHasData() === true) {

		$operator = getDataFromRequest('operation');
		$var1 = getDataFromRequest('var1');
		$var2 = getDataFromRequest('var2');

		$hasErrors = validateData('var1', 'var2');
	return getResult($var1, $var2, $operator);
	}
}