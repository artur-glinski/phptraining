<?php

function showcalculatorMain($templateVars)
{
	$template = getTemplate('/calculatorMain.tmpl.html');

	if (array_key_exists('operation', $_REQUEST) && count($GLOBALS['errors']) === 0) {
		$template = getTemplate('result.tmpl.html');
		$template = str_replace('[[result]]', $templateVars, $template);
	} else {
		if (count($GLOBALS['errors']) === 1) {
			$template = getTemplate('error.tmpl.html');
			$template = str_replace('[[error]]', $GLOBALS['errors'][0], $template);
		} else {
			$template = getTemplate('errors.tmpl.html');
			$template = str_replace('[[error1]]', $GLOBALS['errors'][0], $template);
			$template = str_replace('[[error2]]', $GLOBALS['errors'][1], $template);
		}		
	}
	echo $template;
}

function getTemplate($templateName){
	return file_get_contents(__DIR__ . '/'. $templateName);
}
